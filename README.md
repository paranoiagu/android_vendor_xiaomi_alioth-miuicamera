**This is the Miui Camera Repo for Alioth/Alioth taken from 13.0.8.0 EEA**

***Clone this repo in your vendor xiaomi***

	git clone https://gitlab.com/dark.phnx12/android_vendor_xiaomi_alioth-miuicamera.git vendor/xiaomi/alioth-miuicamera

**Special Thanks to @spkal and @TheSudhirYadav7**


**Special Thanks to  @Devsaee for testing almost all builds**

**Thanks to below pros for helping**

    @johnmart19
    @ZiadFekr
    @The_CatGuy
    @Eidoron1   
    @lazyafk
    @Roxor007

**You need Below chnages on you trees**

***Trees***
        
        https://github.com/Spark-Devices/android_device_xiaomi_alioth/commit/d2eff568a1fd88ad43075c6da89f2fb15ce62277
        
        https://github.com/Spark-Devices/alioth_device_xiaomi_sm8250-common/commit/7531c51fe8d047fb86ac2d2c81d0aed00855fa52

***source***
    
    https://github.com/spark-munch/frameworks_base/commit/07fb81ff3609ee7543fdd3caee339b4e173e05fb   

    note : There maybe more source commits required before compltely using miuicam, you can check spark repo for commits 
            or you can drop 48mp lib and you can use it without 48mp mode reset should work without any issues 

***Few things are broken will try to fix them in future releases, contributions are always welcomed***

***Please report with logs if you found any bug or anything broken***
